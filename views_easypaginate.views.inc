<?php

/* hook_views_plugins */

function views_easypaginate_views_plugins() {
  return array(
      'module' => 'views_easypaginate',
      'style' => array(
          'easypaginate' => array(
              'title' => t('Easy paginate'),
              'handler' => 'views_plugin_style_easypaginate',
              'uses options' => TRUE,
              'help' => t('Render using easypaginate plugin.'),
              'theme' => 'views_easypaginate_easypaginate_view',
              'theme path' => drupal_get_path('module', 'views_easypaginate'),
              'type' => 'normal',
              'uses row plugin' => TRUE,
          ),
      ),
  );
}