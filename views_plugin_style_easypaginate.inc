<?php

class views_plugin_style_easypaginate extends views_plugin_style {

  //default options
  function option_definition() {
    $options = parent::option_definition();

    $options['step'] = array('default' => 4); // Defines a number of items visible on each "page".
    $options['delay'] = array('default' => 100); // Items on each "page" fade in one by one. This parameter controls the pause between each item’s appearance so we can create "wave" effect. It is defined in milliseconds.
    $options['numeric'] = array('default' => true); // Boolean. If set to true then the numeric pagination buttons will show.
    $options['nextprev'] = array('default' => true); // Boolean. If set to true then the next and previous pagination buttons will show.
    $options['auto'] = array('default' => false); // Boolean. If set to true then the plugin will automatically rotate the "pages"
    $options['pause'] = array('default' => 4000); // If set to auto pagination, this parameter controls the length of the pause in milliseconds between each "page turn".
    $options['clickstop'] = array('default' => true); // If set to auto pagination, this parameter controls whether the pages will continue to automatically rotate. If you want to continue the rotation set this parameter to false.
    $options['controls'] = array('default' => 'pagination'); // As mentioned, the plugin generates an ordered list for the purpose of navigating through "pages". This parameter defines the list’s ID.
    $options['current'] = array('default' => 'current'); // This parameter defines a class name of a current "page" in the numeric navigation. It is used for styling purposes.

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['step'] = array(
        '#type' => 'select',
        '#title' => t('Step'),
        '#options' => array(
            3 => 3,
            4 => 4,
            5 => 5,
        ),
        '#required' => TRUE,
        '#description' => t('Defines a number of items visible on each "page".'),
        '#default_value' => $this->options['step'],
    );
    $form['delay'] = array(
        '#type' => 'select',
        '#title' => t('Delay'),
        '#options' => array(
            0 => 0,
            100 => 100,
            200 => 200,
        ),
        '#required' => TRUE,
        '#description' => t('Items on each "page" fade in one by one. This parameter controls the pause between each item’s appearance so we can create "wave" effect. It is defined in milliseconds.'),
        '#default_value' => $this->options['delay'],
    );
    $form['numeric'] = array(
        '#type' => 'checkbox',
        '#title' => t('Numeric'),
        '#description' => t('Boolean. If set to true then the numeric pagination buttons will show.'),
        '#default_value' => $this->options['numeric'],
    );
    $form['nextprev'] = array(
        '#type' => 'checkbox',
        '#title' => t('Next/Prev'),
        '#description' => t('Boolean. If set to true then the next and previous pagination buttons will show.'),
        '#default_value' => $this->options['nextprev'],
    );
    $form['auto'] = array(
        '#type' => 'checkbox',
        '#title' => t('Auto'),
        '#description' => t('Boolean. If set to true then the plugin will automatically rotate the "pages"'),
        '#default_value' => $this->options['auto'],
    );
    $form['pause'] = array(
        '#type' => 'select',
        '#title' => t('Pause'),
        '#options' => array(
            0 => 0,
            1000 => 1000,
            4000 => 4000,
        ),
        '#required' => TRUE,
        '#description' => t('If set to auto pagination, this parameter controls the length of the pause in milliseconds between each "page turn".'),
        '#default_value' => $this->options['pause'],
    );
    $form['clickstop'] = array(
        '#type' => 'checkbox',
        '#title' => t('Click stop'),
        '#description' => t('If set to auto pagination, this parameter controls whether the pages will continue to automatically rotate. If you want to continue the rotation set this parameter to false.'),
        '#default_value' => $this->options['clickstop'],
    );
    $form['controls'] = array(
        '#type' => 'textfield',
        '#title' => t('Controls'),
        '#required' => TRUE,
        '#description' => t('As mentioned, the plugin generates an ordered list for the purpose of navigating through "pages". This parameter defines the list’s ID.'),
        '#default_value' => $this->options['controls'],
    );
    $form['current'] = array(
        '#type' => 'textfield',
        '#title' => t('Current'),
        '#required' => TRUE,
        '#description' => t('This parameter defines a class name of a current "page" in the numeric navigation. It is used for styling purposes.'),
        '#default_value' => $this->options['current'],
    );
  }

  function options_validate(&$form, &$form_state) {
    parent::options_validate($form, $form_state);
    $options = &$form_state['values']['style_options'];
  }

}
