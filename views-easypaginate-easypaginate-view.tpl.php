<div class="views_easypaginate views_easypaginate-<?php print $view->vid; ?> clearfix">
  <?php if (!empty($title)) : ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
  <ul class="item-list clearfix">
    <?php foreach ($rows as $id => $row): ?>
      <li class="<?php print 'item-' . $id; ?>"><?php print $row; ?></li>
    <?php endforeach; ?>
  </ul>
</div>
